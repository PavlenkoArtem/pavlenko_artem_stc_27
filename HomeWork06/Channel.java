import java.util.Random;

public class Channel {
    private Program program;
    private String name;
    private Program[] programArray= new Program[COUNT];

    private static final int COUNT = 5;
    private Random random = new Random();


    public Channel(String name) {
        this.name = name;
    }

    public Program[] addProgramArray() {
        for (int i = 0; i < programArray.length; i++) {
            program = new Program(RamdomName.nameGenerate());
            programArray[i] = program;
        }
        for (int i = 0; i < programArray.length; i++) {
            System.out.println(programArray[i]);

        }
        return programArray;
    }
    public Program randomPrograms(){
        int randoms = random.nextInt(programArray.length);
        program = programArray[randoms];
        System.out.println(program);
        return program;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "name='" + name + '\'' +
                '}';
    }
}
