 public class Program {
    private String name;

     public Program(String progName) {
         this.name = progName;
     }

     @Override
     public String toString() {
         return "Program{" +
                 "name='" + name + '\'' +
                 '}';
     }

     public String getName() {
         return name;
     }
 }
