import java.util.Random;

public class RamdomName {
    private static final int BOUND = 26;
    static Random random = new Random();
    static char letterGen() {
        int dec = random.nextInt(BOUND) + 'a';
        String hexStr = Integer.toHexString(dec);
        return (char) Integer.parseInt(hexStr, 16);
    }

    static String nameGenerate() {
        StringBuffer name = new StringBuffer();
        int nameLength = random.nextInt(6) + 3;
        name.append(String.valueOf(letterGen()).toUpperCase());
        for (int j = 1; j <= nameLength - 1; j++) {
            name.append(letterGen());
        }
        return name.toString();
    }
}
