class Program1{
    public static void main(String[] args) {
        int number = 12345;
        int sum = 0;
        int currentDigit;

        currentDigit = number % 10;   // 1234
        sum = sum + currentDigit;     // 5

        number = number / 10; // 1234

        currentDigit = number % 10;   // 123
        sum = sum + currentDigit;     // 5 + 4 = 9

        number = number / 10;  // 123

        currentDigit = number % 10;   // 12
        sum = sum + currentDigit;     // 9 + 3 = 12

        number = number / 10;        // 12

        currentDigit = number % 10;  // 1
        sum = sum + currentDigit;    // 12 + 2

        number = number / 10;

        currentDigit = number % 10; // 1
        sum = sum + currentDigit;  // 14 + 1

        System.out.println(sum);    // 15
    }
}