import java.util.Scanner;

public class Program3 {
    public static int sumPrimeNumber(int num) {
        int sum = 0;
        while (num != 0) {
            sum += (num % 10);
            num /= 10;
        }
        return sum;
    }

    public static boolean isPrime(int num) {
        if (num < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long result = 0;
        int cursor = 1;

        while (cursor != 0) {
            int element = scanner.nextInt();

            if (isPrime(sumPrimeNumber(element))) {

                if (result == 0) {
                    result = 1;
                }
                result *= element;

            }
            cursor = element;

        }

        System.out.println(result);
    }
}
