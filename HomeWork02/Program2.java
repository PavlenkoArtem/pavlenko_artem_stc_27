import java.util.Scanner;
import java.util.Arrays;
class Program2{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        int min = 0;
        int max = 0;
        int tmp;
        for(int i = 0; i < n; i++){
            array[i] = scanner.nextInt();
            if ( array[min] > array[i] ) min = i;
            if ( array[max] < array[i] ) max = i;
        }
        System.out.println(Arrays.toString(array));
        System.out.println("Min: "+"array["+min+"]="+array[min]);
        System.out.println("Max: "+"array["+max+"]="+array[max]);

        temp = array[min];
        array[min] = array[max];
        array[max] = temp;

        System.out.println(Arrays.toString(array));
    }

    }
}