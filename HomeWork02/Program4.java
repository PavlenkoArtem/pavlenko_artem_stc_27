import java.util.Arrays;

class Program4 {
    public static void main(String[] args) {
        int[] array = {4, 2, 3, 5, 7, 1};
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }

        }
        System.out.println(Arrays.toString(array));

    }
}