import java.util.Scanner;
import java.util.Arrays;
class Program{
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            int array[] = new int[n];
            for(int i = 0; i < n; i++){

                array[i] = scanner.nextInt();
            }
            System.out.println(Arrays.toString(array));

            System.out.println(Arrays.toString(getResult(array)));
        }

        static int[] getResult(int[] array) {
            int temp;
            for (int i = array.length-1, j = 0; i >=array.length/2 ; i--,j++) {
                temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
            return array;
        }
    }
}