package com.company;

public class Ellipse extends Figure {

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public Ellipse(double a) {
        this.a = a;
        sumSpace();
        sumPerimeter();
    }

    public Ellipse(double a, double b) {
        this.a = a;
        this.b = b;
        while (a <= 0 || b <= 0) {
            System.out.println("Введено не допустимое значение = 0");
            break;
        }
        if (a > 0 && b > 0) {
            sumSpace();
            sumPerimeter();
            System.out.println("Площадь: " + getSpace());
            System.out.println("Периметр: " + getPerimeter());
        }
        this.setXCenter(0.0);
        this.setYCenter(0.0);
    }

    public void sumSpace() {
        this.setSpace(a * b * Math.PI);
    }

    public void sumPerimeter() {
        this.setPerimeter(4 * (Math.PI * a * b + (a - b)) / a + b);
    }

    @Override
    public void uvelichenie(int num) {
        this.setA(a * num);
        this.setB(b * num);
    }

    @Override
    public void move(double x, double y) {
        setXCenter(x);
        setYCenter(y);
    }

}
