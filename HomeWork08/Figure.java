package com.company;

public abstract class Figure implements Scalable, Movable {
    protected double a, b;
    private double space;
    private double perimeter;
    private double xCenter;
    private double yCenter;

    public double getSpace() {
        return space;
    }

    public void setSpace(double space) {
        this.space = space;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }

    public void setXCenter(double xCenter) {
        this.xCenter = xCenter;
    }

    public void setYCenter(double yCenter) {
        this.yCenter = yCenter;
    }

}
