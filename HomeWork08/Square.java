package com.company;

public class Square extends Rectangle {

    public void setA(double a) {
        this.a = a;
    }

    public Square(int a) {
        super(a);
        while (a <= 0) {
            System.out.println("Введено не допустимое значение = 0");
            break;
        }
        if (a > 0) {
            sumSpace();
            System.out.println("Площадь квадрата: " + getSpace());
            System.out.println("Периметр квадрата: " + getPerimeter());
        }
        this.setXCenter(0.0);
        this.setYCenter(0.0);
    }

    @Override
    public void sumSpace() {
        this.setSpace(a * a);
    }

    @Override
    public void sumPerimeter() {
        this.setPerimeter(4 * a);
    }

    @Override
    public void uvelichenie(int num) {
        this.setA(a * num);
        System.out.println("Масштабируемый увеличивает в: " + num + " раза");
    }

    @Override
    public void move(double x, double y) {
        setXCenter(x);
        setYCenter(y);
        System.out.println("Изменение координат в интерфейсе Перемещаемый: " + "x: " + x + " , " + " y: " + y);

    }
}
