package com.company;

public class Circle extends Ellipse {


    public void setRadius(double radius) {
        this.a = radius;
    }

    public Circle(double radius) {
        super(radius);
        while (a <= 0) {
            System.out.println("Введено не допустимое значение = 0");
            break;
        }
        if (radius > 0) {
            sumSpace();
            System.out.println("Площадь круга равна: " + getSpace());
            System.out.println("Длины окружности равна: " + getPerimeter());

        }
        this.setXCenter(0.0);
        this.setYCenter(0.0);
    }

    @Override
    public void sumSpace() {
        this.setSpace(Math.PI * (a * a));
    }

    @Override
    public void sumPerimeter() {
        this.setPerimeter(2 * Math.PI * a);
    }

    @Override
    public void uvelichenie(int num) {
        this.setRadius(a * num);
    }

    @Override
    public void move(double x, double y) {
        setXCenter(x);
        setYCenter(y);
    }
}
