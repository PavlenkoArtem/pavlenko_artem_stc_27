package com.company;

public class Rectangle extends Figure {

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public Rectangle(double a) {
        this.a = a;
        this.sumSpace();
        this.sumPerimeter();

    }

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
        while (a <= 0 || b <= 0) {
            System.out.println("Введено не допустимое значение = 0");
            break;
        }
        if (a > 0 && b > 0) {
            sumSpace();
            sumPerimeter();
            System.out.println("Площадь Прямоугольника: " + getSpace());
            System.out.println("Периметр Прямоугольника: " + getPerimeter());
        }
        this.setXCenter(0.0);
        this.setYCenter(0.0);
    }

    public void sumSpace() {
        this.setSpace(a * b);
    }

    public void sumPerimeter() {
        this.setPerimeter(2 * (a + b));
    }

    @Override
    public void uvelichenie(int num) {
        this.setA(a * num);
        this.setB(b * num);
    }

    @Override
    public void move(double x, double y) {
        setXCenter(x);
        setYCenter(y);
    }
}
