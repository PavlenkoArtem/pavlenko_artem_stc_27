package com.company;

public class Main {

    public static void main(String[] args) {
        Circle circle = new Circle(1);
        Ellipse ellipse = new Ellipse(1, 2);
        Rectangle rectangle = new Rectangle(3, 5);
        Square square = new Square(3);
        circle.move(1.0, 2.3);
        circle.uvelichenie(2);
        ellipse.move(1.0,2.0);
        ellipse.uvelichenie(2);
        rectangle.move(1.0,2.0);
        rectangle.uvelichenie(2);
        square.move(1.0,2.0);
        square.uvelichenie(2);
    }
}
