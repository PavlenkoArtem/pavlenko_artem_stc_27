package com.company;

public interface Movable {
    void move(double x, double y);
}
