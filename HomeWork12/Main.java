package com.company;

public class Main {

    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1,"Один");
        map.put(2,"Два");
        map.put(3,"Три");
        map.put(4,"Четыре");
        map.put(5,"Пять");
        map.put(6,"Шесть");

        System.out.println(map.get(6));
    }
}
