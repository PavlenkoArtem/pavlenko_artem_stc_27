public class Program1 {
    public static int recursion(double num) {
        if (num == 1) {
            return 1;
        } else if (num > 1 && num < 2) {
            return 0;
        } else {
            return recursion(num / 2);
        }
    }

    public static void main(String[] args) {
        double num = 9;
        if (recursion(num) == 1) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
}