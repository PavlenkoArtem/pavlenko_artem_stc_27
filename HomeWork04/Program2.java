public class Program2 {
    public static int binarySearch(int[] array, int element) {
        int start = 0;
        int end = array.length - 1;
        boolean toggle = true;
        while (toggle) {
            int mid = (start + end) / 2;
            if (array[mid] == element) {
                System.out.println(mid);
                toggle = false;

            } else {
                if (array[mid] < element) {
                    start = mid + 1;
                } else {
                    end = mid - 1;

                }

            }
        }while (toggle) {
            return binarySearch(array, element);

        }
        return element;
    }

    public static void main(String[] args) {
        System.out.println(binarySearch(new int[]{4, 6, 8, 10, 13, 17, 34, 53, 67, 84, 92, 99}, 84));
    }
}