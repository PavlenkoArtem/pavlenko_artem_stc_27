public class Program3 {
    public static int fib1(int n, int a, int b) {
        if (n <= 2) {
            return a + b;
        }
        return fib1(n - 1, b, a + b);
    }

    public static void main(String[] args) throws java.lang.Exception {
        for (int i = 1; i < 10; i++) {
            System.out.println(fib1(i, 0, 1));
        }
    }
}