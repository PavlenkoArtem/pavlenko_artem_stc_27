package com.company;

public class Main {

    public static void main(String[] args) {

        int[] numbers = {120345,50502,7203405};
        String[] strings = {"A1bC3dE6f", "A2b3d4c5", "a1Bcd2e"};

        NumbersAndStringProcessor nsp = new NumbersAndStringProcessor(numbers, strings);

        Result result = new Result();
        result.resultLamb(nsp);

    }
}
