package com.company;

public class NumbersAndStringProcessor {
    public int[] number;
    public String[] string;

    public NumbersAndStringProcessor(int[] number,String[] string) {
        this.number = number;
        this.string = string;
    }

    public int[] process(NumberProcess process) {
        int[] numbers = new int[number.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = process.process(number[i]);
        }
        return numbers;

    }

    public String[] process(StringProcess process) {
        String[] strings = new String[string.length];
        for (int i = 0; i < strings.length; i++) {
         strings[i] = process.process(string[i]);
        }
        return strings;

    }

}
