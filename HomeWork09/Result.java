package com.company;

import java.util.Arrays;

public class Result {

    public static void resultLamb(NumbersAndStringProcessor nsp) {

        NumberProcess Numbers = number -> {
            int result;

            result = number;

            return result;
        };

        StringProcess Strings = str -> {
            String result = "";

            result = str;

            return result;
        };

        NumberProcess reverseNumber = number -> {
            int reverse = 0;
            while (number != 0) {
                reverse = reverse * 10;
                reverse = reverse + number % 10;
                number = number / 10;
            }
            return reverse;
        };
        NumberProcess removeZeros = number -> {
            if (number != 0) {
                return Integer.parseInt(Integer.toString(number).replace("0", ""));
            }
            return 0;
        };
        NumberProcess odd2evan = number -> {
            StringBuilder result = new StringBuilder();
            while (number > 0) {
                int firstDigit = number % 10;

                if (firstDigit % 2 != 0)
                    --firstDigit;
                result.append(firstDigit);

                number = number / 10;
            }
            return Integer.parseInt(result.reverse().toString());
        };
        StringProcess reverseString = string -> {

            StringBuilder sb = new StringBuilder(string);
            return String.valueOf(sb.reverse());
        };
        StringProcess removeNumOfStr = string -> {
            if (string == null) {
                return null;
            }
            char[] ch = string.toCharArray();
            int length = ch.length;
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < length) {
                if (Character.isDigit(ch[i])) {
                    i++;
                } else {
                    sb.append(ch[i]);
                    i++;
                }
            }
            return sb.toString();
        };
        StringProcess upFirstChar = string -> {
            String s = string;
            return s.toUpperCase();
        };
        System.out.println("Исходные числа " + Arrays.toString(nsp.process(Numbers)) +
                " ; " + "Исходные строки " + Arrays.toString(nsp.process(Strings)) +
                "\n\nИнверсия чисел " + Arrays.toString(nsp.process(reverseNumber)) +
                "\nУбрать нули из исходных чисел " + Arrays.toString(nsp.process(removeZeros)) +
                "\nЗаменить нечетные цифры ближайшей четной снизу " + Arrays.toString(nsp.process(odd2evan)) +
                "\n\nИнверсия строк " + Arrays.toString(nsp.process(reverseString)) +
                "\nУбрать все цифры из строк " + Arrays.toString(nsp.process(removeNumOfStr)) +
                "\nСделать все маленькие буквы большими " + Arrays.toString(nsp.process(upFirstChar)));
    }
}
