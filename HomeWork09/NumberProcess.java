package com.company;

@FunctionalInterface
public interface NumberProcess {

    int process(int number);

}

