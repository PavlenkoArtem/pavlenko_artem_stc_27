package my.gamepackage.repositories;
import my.gamepackage.models.User;

public interface PlayersRepository {
    void save(User user);
    void update(User user);
    User findByNickname(String nickName);

}
