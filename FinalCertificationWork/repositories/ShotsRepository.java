package my.gamepackage.repositories;

import my.gamepackage.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
