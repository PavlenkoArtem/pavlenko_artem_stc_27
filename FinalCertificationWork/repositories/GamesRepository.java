package my.gamepackage.repositories;
import my.gamepackage.models.Game;

public interface GamesRepository {
    void save(Game game);
    Game findById(long id);
    void update(Game game);
}
