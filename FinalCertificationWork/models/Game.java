package my.gamepackage.models;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

public class Game {

    // ID игры....
    private long id;
    // Дата игры....
    private LocalDate date;
    //Все зарегистрированные Игроки....
    private Set<User> users;
    // Сколько было выстрелов от каждого игрока....
    private Map<User, Integer> shots;
    // Сколько длилась игра....
    private Duration game;

}
