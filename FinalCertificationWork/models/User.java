package my.gamepackage.models;

public class User {

    // ID пользователя....
    long id;
    // IP-адрес пользователя.....
    private String userIP;
    // Имя пользователя....
    private String nickName;
    // Максимальное количество очков пользователя....
    private long Max_point;
    // Количество побед пользователя....
    private int win;
    // Количество  поражений пользователя....
    private int loss;

}
