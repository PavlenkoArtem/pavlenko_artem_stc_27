import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static int arraySum(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        return sum;
    }

    static int[] returnReversArray(int[] arr) {
        int j = 0;
        int[] res = new int[arr.length];
        for (int i = arr.length - 1; i >= 0; i--, j++) {
            res[j] = arr[i];
            System.out.print(res[j] + " ");
        }
        return res;
    }

    public static double arithmeticMean(int[] array) {
        double average = 0;
        if (array.length > 0) {
            int sum = 0;
            for (int i = 0; i < array.length; i++) {
                sum += array[i];
            }
            average = sum / array.length;
        }
        return average;
    }

    public static int[] minMaxArray(int[] array) {
        int min = 0, max = 0;
        for (int i = 1; i < array.length; ++i) {
            if (array[i] < array[min])
                min = i;
            if (array[i] > array[max])
                max = i;
        }
        int t;
        if (max != min) {
            t = array[min];
            array[min] = array[max];
            array[max] = t;

        }
        return array;

    }

    public static int arrayToInt(int[] arr) {
        int n = 0;
        for (int d : arr) {
            n = 10 * n + d;
        }
        return n;
    }

    public static void bubbleSorter(int[] a) {

        boolean flag = true;
        while (flag) {
            flag = false;
            for (int j = 0; j < a.length - 1; j++) {
                if (a[j] > a[j + 1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    flag = true;
                }
            }
        }

    }

    public static void main(String[] args) {
        int[] a = new int[]{3, 6, 2, 10, 1};
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[5];

        for (int i = 0; i < 5; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Задание №1");
        System.out.println(arraySum(new int[]{1,2,3,4,5,6}));
        System.out.println("Задание №2");
        System.out.println(returnReversArray(array));
        System.out.println("Задание №3");
        System.out.println(arithmeticMean(array));
        System.out.println("Задание №4");
        for (int i : minMaxArray(new int[]{1,2,3,4,5,6,7})) System.out.print(" " + i);
        System.out.println();
        System.out.println("Задание №5");
        System.out.println(arrayToInt(new int[]{1,3,4,5,6}));
        System.out.println("Задание №6");
        System.out.println("массив перед пузырьковой сортировкой : " + Arrays.toString(a));
        bubbleSorter(a);
        System.out.println("массив после пузырьковой сортировки : " + Arrays.toString(a));
    }
}
