package my.homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;

public class ParsingCar {

    public static void parsingCar() {
        try (BufferedReader br = new BufferedReader(new FileReader("avto.txt"))) {
            br.lines().map(s -> {
                String[] array = s.split("\\s+");
                return new Cars(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4]));
            }).filter(cars -> cars.getColorCar().equals("чёрный") || cars.getMileage() == 0)
                    .forEach(cars -> System.out.println("Номера автомобилей, имеющих черный цвет или нулевой пробег. = " + cars.getNumCar()));
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
        try (BufferedReader br = new BufferedReader(new FileReader("avto.txt"))) {
            Long countCarByDiapason = br.lines().map(s -> {
                String[] array = s.split("\\s+");
                return new Cars(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4]));
            }).filter(cars -> cars.getPriceCar() >= 700000L && cars.getPriceCar() <= 800000L)
                    .map(cars -> cars.getModel()).distinct().count();
            System.out.println("\n" + "Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. = " + countCarByDiapason + "\n");
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
        try (BufferedReader br = new BufferedReader(new FileReader("avto.txt"))) {
            String colorCar = br.lines().map(s -> {
                String[] array = s.split("\\s+");
                return new Cars(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4]));
            })
                    .min(Comparator.comparingLong(Cars::getPriceCar)).get().getColorCar();
            System.out.println("Цвет автомобиля с минимальной ценной. = " + colorCar + "\n");
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }

        try (BufferedReader br = new BufferedReader(new FileReader("avto.txt"))) {
            Double averagePrice = br.lines().map(s -> {
                String[] array = s.split("\\s+");
                return new Cars(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4]));
            })
                    .filter(cars -> cars.getModel().equals("Toyota_Camry"))
                    .mapToLong(Cars::getPriceCar).average().getAsDouble();
            System.out.println(" Средняя стоимость Toyota Camry. = " + averagePrice);
        } catch (IOException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
}


