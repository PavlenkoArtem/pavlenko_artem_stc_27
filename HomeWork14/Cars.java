package my.homework;

public class Cars {
    private String numCar;
    private String model;
    private String colorCar;
    private long mileage;
    private long priceCar;

    public Cars(String numCar, String model, String colorCar, long mileage, long priceCar) {
        this.numCar = numCar;
        this.model = model;
        this.colorCar = colorCar;
        this.mileage = mileage;
        this.priceCar = priceCar;
    }

    public String getNumCar() {
        return numCar;
    }

    public String getModel() {
        return model;
    }

    public String getColorCar() {
        return colorCar;
    }

    public long getMileage() {
        return mileage;
    }

    public long getPriceCar() {
        return priceCar;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "numCar='" + numCar + '\'' +
                ", model='" + model + '\'' +
                ", colorCar='" + colorCar + '\'' +
                ", mileage=" + mileage +
                ", priceCar=" + priceCar +
                '}';
    }
}
