package com.company;

public class Main {
    public static void main(String[] args) {
        User user = User.newBuilder()
                .setFirstName("Artem")
                .setLostName("Pavlenko")
                .setAge(34)
                .setIsWorker(true)
                .build();
        user.print();
    }

    public static class User {
        private String firstName;
        private String lostName;
        private int age;
        private boolean isWorker;

        public void print() {
            System.out.println(getFirstName() + " " + getLostName() + " " + getAge() + " " + getIsWorker());
        }

        private User() {
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLostName() {
            return lostName;
        }

        public int getAge() {
            return age;
        }

        public boolean getIsWorker() {
            return isWorker;
        }

        public static User.Builder newBuilder() {
            return new User().new Builder();
        }

        public class Builder {

            private Builder() {

            }

            public User.Builder setFirstName(String firstName) {
                User.this.firstName = firstName;
                return this;
            }

            public User.Builder setLostName(String lostName) {
                User.this.lostName = lostName;
                return this;
            }

            public User.Builder setAge(int age) {
                User.this.age = age;
                return this;
            }

            public User.Builder setIsWorker(boolean isWorker) {
                User.this.isWorker = isWorker;
                return this;
            }

            public User build() {

                return User.this;
            }

        }
    }

}

