package com.company;

public class Main {

    public static void main(String[] args) {
        List arraylist = new ArrayList();
        arraylist.add(1);
        arraylist.add(2);
        arraylist.add(3);
        arraylist.add(4);
        arraylist.add(5);
        arraylist.add(6);

        List linkedlist = new LinkedList();
        linkedlist.add(11);
        linkedlist.add(12);
        linkedlist.add(13);
        linkedlist.add(14);
        linkedlist.add(15);
        linkedlist.add(16);

        System.out.println(arraylist.contains(1));
        arraylist.removeByIndex(1);
        arraylist.remove(3);

        Iterator iteratorArray = arraylist.iterator();
        while (iteratorArray.hasNext()){
            System.out.println(iteratorArray.next());
        }
        System.out.println();
        System.out.println(linkedlist.contains(11));
        linkedlist.removeByIndex(0);
        linkedlist.remove(13);


        Iterator iteratorLinked = linkedlist.iterator();
        while (iteratorLinked.hasNext()){
            System.out.println(iteratorLinked.next());
        }

    }

}
