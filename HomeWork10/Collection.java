package com.company;

public interface Collection {

    void add(int element);

    int size();

    boolean contains(int element);

    void removeByIndex(int index);

    void remove(int element);

    Iterator iterator();
}
