package com.company;

import java.util.Arrays;

public class ArrayList implements List {
    private static final int DEFAULT_ARRAYLIST_SIZE = 10;

    public int[] elements;

    public int size;

    public ArrayList() {
        this.elements = new int[DEFAULT_ARRAYLIST_SIZE];
        this.size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(int element) {
        this.elements[size] = element;
        size++;
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void removeByIndex(int index) {
        if(index >= 0 && index < size){
            System.arraycopy(elements,index + 1,elements,index,size - index - 1);
            size--;
        }

    }

    @Override
    public void remove(int element) {
        for (int i = 0; i < size; i++) {
           if (elements[i] == element) {
               System.arraycopy(elements,element + 1,elements,element,size - element - 1);
               size--;
           }

        }
        
    }
    private class ArrayListIterator implements Iterator{

        private int current = 0;

        @Override
        public int next() {
           int element = elements[current];
           current++;
            return element;
        }

        @Override
        public boolean hasNext() {
            return current < size;
        }
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            return elements[index];
        } else
            System.out.println("Index uot of bounds");
        return -1;
    }

    @Override
    public void addFirst(int element) {
        this.elements[size] = element;
        size++;

        for (int i = size + 1; i > 0; i--) {
            this.elements[i] = this.elements[i - 1];
        }
        this.elements[0] = element;
        size++;
    }

    @Override
    public String toString() {
        return "ArrayList{" +
                "elements=" + Arrays.toString(elements) +
                ", size=" + size +
                '}';
    }
}
