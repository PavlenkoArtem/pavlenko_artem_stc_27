package com.company;


public class LinkedList implements List {


    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;
    private int size;

    private class LinkedListIterator implements Iterator {
        private int current = 0;

        @Override
        public int next() {
            int value = get(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < size;
        }

    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);

        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        size++;
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean contains(int element) {
        Node node = first;
        for (int i = 0; i < size; i++) {
            if (node.value == element) {
                return true;
            }
            node = node.next;
        }
        return false;
    }

    @Override
    public void removeByIndex(int index) {
        if (first == null) {
            return ;
        }
        if (index == 0) {
            first = first.next;
            size--;
            return;
        }
        Node current = first;
        for (int i = 0; i < size; i++) {
            if (i == index){
              current.next = null;
              size--;
              return;
            }
            current = current.next;
        }
    }



    @Override
    public void remove(int element) {
        if (first == null) {
            return;
        }
        if (first.value == element) {
            first = first.next;
            size--;
            return;
        }
        Node current = first;
        while (current.next != null) {
            if (current.next.value == element) {
                current.next = current.next.next;
                size--;
                return;
            }
            current = current.next;
        }
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            Node current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        System.out.println("Index uot of bounds");
        return -1;
    }

    @Override
    public void addFirst(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            add(element);
        } else {
            newNode.next = first;
            first = newNode;
            size++;
        }
    }

    @Override
    public String toString() {
        return "LinkedList{" +
                "first=" + first +
                ", last=" + last +
                ", size=" + size +
                '}';
    }
}
